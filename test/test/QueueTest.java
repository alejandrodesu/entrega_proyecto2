package test;
import junit.framework.TestCase;
import model.data_structures.*;

public class QueueTest extends TestCase 
{
	private Queue<String> cola;
	
	private void setupEscenario1()
	{
		try
		{
			cola = new Queue<String>();
		}
		catch(Exception E)
		{
			fail("No deberia lanzar Excepci�n");
		}
	}
	
	private void setupEscenario2()
	{
		setupEscenario1();
		cola.enqueue("A");
	}
	
	private void setupEscenario3()
	{
		setupEscenario2();
		cola.enqueue("B");
		cola.enqueue("C");
	}
	
	public void testEnqueue()
	{
		setupEscenario3();
		assertEquals("A", cola.darElemento(0));
		assertEquals("B", cola.darElemento(1));
		assertEquals("C", cola.darElemento(2));
	}
	
	public void testDarelemento()
	{
		setupEscenario3();
		assertEquals("A", cola.darElemento(0));
		assertEquals("B", cola.darElemento(1));
		assertEquals("C", cola.darElemento(2));
	}
	
	public void testDequeue()
	{
		setupEscenario3();
		assertEquals("A", cola.dequeue());
		assertEquals("B", cola.dequeue());
		assertEquals("C", cola.dequeue());
	}
}

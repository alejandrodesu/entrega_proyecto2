package test;
import junit.framework.TestCase;
import model.data_structures.*;

public class ColaPrioridadTest extends TestCase {
	
	private ColaPrioridad<String> cola;
	
	private void setupEscenario1()
	{
		try
		{
			cola = new ColaPrioridad<String>();
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
	private void setupEscenario2()
	{
		try
		{
			cola = new ColaPrioridad<String>();
			cola.insert("A");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario3()
	{
		try
		{
			cola = new ColaPrioridad<String>(3);
			cola.insert("D");
			cola.insert("K");
			cola.insert("V");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario4()
	{
		try
		{
			cola = new ColaPrioridad<String>(5);
			cola.insert("K");
			cola.insert("A");
			cola.insert("M");
			cola.insert("L");
			cola.insert("C");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	public void testmax()
	{
		setupEscenario2();
		assertEquals("A", cola.max());
		setupEscenario3();
		assertEquals("V", cola.max());
		setupEscenario4();
		assertEquals("M", cola.max());
	}
	
	public void testSize()
	{
		setupEscenario4();
		assertEquals(5, cola.size());
	}
	
	public void testdelMax()
	{
		setupEscenario4();
		assertEquals("M", cola.delMax());
		assertEquals("L", cola.delMax());
		assertEquals("K", cola.delMax());
		assertEquals("C", cola.delMax());
		assertEquals("A", cola.delMax());
	}
}

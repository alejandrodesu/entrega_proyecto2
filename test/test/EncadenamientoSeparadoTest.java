package test;
import junit.framework.TestCase;
import model.data_structures.*;

public class EncadenamientoSeparadoTest extends TestCase
{
	private EncadenamientoSeparado<String, Integer> hash1;
	
	private void setupEscenario1(){
		try
		{
			EncadenamientoSeparado<String, Integer> hash1 = new EncadenamientoSeparado<>(1);
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
			
	}
	
	private void setupEscenario2()
	{
		try
		{
			EncadenamientoSeparado<String, Integer> hash1 = new EncadenamientoSeparado<>(1);
			hash1.insertar("A", 1);
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
	public void setupEscenario3()
	{
		try
		{
			EncadenamientoSeparado<String, Integer> hash1 = new EncadenamientoSeparado<>(3);
			hash1.insertar("A", 1);
			hash1.insertar("B", 2);
			hash1.insertar("C", 3);
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
	public void setupEscenario4()
	{
		try
		{
			EncadenamientoSeparado<String, Integer> hash1 = new EncadenamientoSeparado<>(5);
			hash1.insertar("A", 1);
			hash1.insertar("B", 2);
			hash1.insertar("C", 3);
			hash1.insertar("K", 4);
			hash1.insertar("P", 5);
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
//	public void testEstaVacia() 
//	{
//		setupEscenario1();
//		assertTrue("Deberia ser true", hash1.estaVacia());
//	}
	
	public void testInsertar()
	{
		setupEscenario2();
		assertEquals("A", hash1.contiene("A"));
	}
	
	public void testContiene()
	{
		setupEscenario2();
		assertEquals("B", hash1.contiene("B"));
		assertEquals("C", hash1.contiene("C"));
	}
	
}

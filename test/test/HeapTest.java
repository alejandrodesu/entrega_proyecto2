package test;
import junit.framework.TestCase;
import model.data_structures.*;

public class HeapTest extends TestCase {
	private Heap<String> heap;
	private String[] baseArr = new String[10];
	private String[] vector;
	private void setupEscenario1()
	{
		try
		{
			heap = new Heap<String>();
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
	private void setupEscenario2()
	{
		try
		{
			heap = new Heap<String>();
			heap.insert("A");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario3()
	{
		try
		{
			heap = new Heap<String>();
			heap.insert("D");
			heap.insert("K");
			heap.insert("V");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario4()
	{
		try
		{
			heap = new Heap<String>();
			heap.insert("K");
			heap.insert("A");
			heap.insert("M");
			heap.insert("L");
			heap.insert("C");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	public void testSize()
	{
		setupEscenario1();
		assertEquals(0, heap.size());
		setupEscenario2();
		assertEquals(1, heap.size());
		setupEscenario3();
		assertEquals(3, heap.size());
	}
	
	public void testInsert()
	{
		setupEscenario2();
		assertEquals("A", heap.peek());
		setupEscenario3();
		assertEquals("D", heap.peek());
	}
	
	public void deleteMin()
	{
		setupEscenario3();
		assertEquals("D", heap.deleteMin());
		setupEscenario4();
		assertEquals("A", heap.deleteMin());
		assertEquals("C", heap.deleteMin());
		assertEquals("K", heap.deleteMin());
		assertEquals("L", heap.deleteMin());
		assertEquals("M", heap.deleteMin());
	}
	
}
